package application

import (
	"fmt"
	"log"
	"mamazon/order/src/database/postgresql"
	"mamazon/order/src/handler"
	"mamazon/order/src/order"
	"mamazon/order/src/utils/config"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
)

func Setup() {

	postgresql.Migrate(order.Order{})

	app := fiber.New()

	app.Use(cors.New())

	order := app.Group("/order")

	order.Post("/", handler.Create)
	order.Get("/by-order/:orderId", handler.GetById)
	order.Get("/by-user/:userId", handler.GetByUserId)
	order.Get("/by-book/:bookId", handler.GetByBookId)

	log.Fatal(app.Listen(fmt.Sprintf(":%v", config.PORT)))
}
