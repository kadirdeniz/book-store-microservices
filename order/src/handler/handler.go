package handler

import (
	"fmt"
	"mamazon/order/src/order"
	orderService "mamazon/order/src/order/service"

	"github.com/gofiber/fiber/v2"
)

func Create(c *fiber.Ctx) error {

	orderService := orderService.New()

	_order := order.Order{}

	if err := c.BodyParser(&_order); err != nil {
		fmt.Println(err)
		return c.Status(fiber.ErrBadRequest.Code).JSON(fiber.Map{
			"status": false,
			"error":  err,
		})
	}
	fmt.Println(string(c.Body()))
	fmt.Println(_order)

	if _order.GetBookId() == 0 || _order.GetUserId() == 0 {
		return c.Status(fiber.ErrBadRequest.Code).JSON(fiber.Map{
			"status": false,
			"error":  "All Fields Must Be Filled",
		})
	}

	order, createErr := orderService.Create(_order.GetUserId(), _order.GetBookId())
	if createErr != nil {
		return c.Status(createErr.StatusCode).JSON(fiber.Map{
			"status": false,
			"error":  createErr,
		})
	}
	return c.JSON(fiber.Map{
		"status":  true,
		"message": "Order Created",
		"data":    order,
	})
}

func GetById(c *fiber.Ctx) error {

	orderService := orderService.New()

	id, paramsErr := c.ParamsInt("orderId")
	if paramsErr != nil {
		return c.Status(fiber.ErrBadRequest.Code).JSON(fiber.Map{
			"status": false,
			"error":  "Parameter Should Be Integer",
		})
	}

	order, getErr := orderService.GetById(id)
	if getErr != nil {
		return c.Status(getErr.StatusCode).JSON(fiber.Map{
			"status": false,
			"error":  getErr,
		})
	}
	return c.JSON(fiber.Map{
		"status":  true,
		"message": "Got Order",
		"data":    order,
	})
}

func GetByUserId(c *fiber.Ctx) error {

	orderService := orderService.New()

	id, paramsErr := c.ParamsInt("userId")
	if paramsErr != nil {
		return c.Status(fiber.ErrBadRequest.Code).JSON(fiber.Map{
			"status": false,
			"error":  "Parameter Should Be Integer",
		})
	}

	order, getErr := orderService.GetByUserId(id)
	if getErr != nil {
		return c.Status(getErr.StatusCode).JSON(fiber.Map{
			"status": false,
			"error":  getErr,
		})
	}
	return c.JSON(fiber.Map{
		"status":  true,
		"message": "Got Order",
		"data":    order,
	})
}

func GetByBookId(c *fiber.Ctx) error {

	orderService := orderService.New()

	id, paramsErr := c.ParamsInt("bookId")
	if paramsErr != nil {
		return c.Status(fiber.ErrBadRequest.Code).JSON(fiber.Map{
			"status": false,
			"error":  "Parameter Should Be Integer",
		})
	}

	order, getErr := orderService.GetByBookId(id)
	if getErr != nil {
		return c.Status(getErr.StatusCode).JSON(fiber.Map{
			"status": false,
			"error":  getErr,
		})
	}
	return c.JSON(fiber.Map{
		"status":  true,
		"message": "Got Order",
		"data":    order,
	})
}
