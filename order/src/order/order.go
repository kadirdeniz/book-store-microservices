package order

import "gorm.io/gorm"

type Order struct {
	gorm.Model
	UserId int `json:"userId"`
	BookId int `json:"bookId"`
}

func New(userId, bookId int) Order {
	return Order{
		UserId: userId,
		BookId: bookId,
	}
}

func (o Order) SetUserId(id int) {
	o.UserId = id
}

func (o Order) SetBookId(id int) {
	o.BookId = id
}

func (o Order) GetUserId() int {
	return o.UserId
}

func (o Order) GetBookId() int {
	return o.BookId
}
