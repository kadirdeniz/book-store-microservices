package service

import (
	"mamazon/order/src/order"
	"mamazon/order/src/utils/error"
)

type IService interface {
	Create(userId, bookId int) (*order.Order, *error.Error)
	GetByBookId(bookId int) (*[]order.Order, *error.Error)
	GetByUserId(userId int) (*[]order.Order, *error.Error)
	GetById(id int) (*order.Order, *error.Error)
}
