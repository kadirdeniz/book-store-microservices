package service

import (
	"mamazon/order/src/order"
	"mamazon/order/src/order/repository"
	"mamazon/order/src/utils/error"
)

type Service struct {
	Repo repository.IRepository
}

func New() IService {
	return Service{
		Repo: repository.New(),
	}
}

func (s Service) Create(userId, bookId int) (*order.Order, *error.Error) {
	order, err := s.Repo.Create(userId, bookId)
	if err != nil {
		return nil, err
	}
	return order, nil
}

func (s Service) GetById(id int) (*order.Order, *error.Error) {
	order, err := s.Repo.GetById(id)
	if err != nil {
		return nil, err
	}
	return order, nil
}

func (s Service) GetByBookId(bookId int) (*[]order.Order, *error.Error) {
	orders, err := s.Repo.GetByBookId(bookId)
	if err != nil {
		return nil, err
	}
	return orders, nil
}

func (s Service) GetByUserId(userId int) (*[]order.Order, *error.Error) {
	orders, err := s.Repo.GetByUserId(userId)
	if err != nil {
		return nil, err
	}
	return orders, nil
}
