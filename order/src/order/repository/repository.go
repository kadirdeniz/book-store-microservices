package repository

import (
	"mamazon/order/src/database/postgresql"
	"mamazon/order/src/order"
	"mamazon/order/src/utils/error"

	"gorm.io/gorm"
)

type Repository struct {
	Repo *gorm.DB
}

func New() IRepository {
	DB, err := postgresql.Connect()
	if err != nil {
		panic(err)
	}
	return Repository{
		Repo: DB,
	}
}

func (r Repository) Create(userId, bookId int) (*order.Order, *error.Error) {

	_order := order.New(userId, bookId)
	response := r.Repo.Create(&_order)

	if response.Error != nil {
		return nil, error.BadRequest("Error Occured While Creating Error")
	}
	return &_order, nil
}

func (r Repository) GetByBookId(bookId int) (*[]order.Order, *error.Error) {

	_orders := []order.Order{}
	response := r.Repo.Where("book_id=?", bookId).Find(&_orders)

	if response.RowsAffected == 0 {
		return nil, error.NotFound("Order Not Found")
	}
	if response.Error != nil {
		return nil, error.ServerError("Error Occured While Getting Order")
	}

	return &_orders, nil
}

func (r Repository) GetByUserId(userId int) (*[]order.Order, *error.Error) {

	_orders := []order.Order{}
	response := r.Repo.Where("user_id=?", userId).Find(&_orders)

	if response.RowsAffected == 0 {
		return nil, error.NotFound("Order Not Found")
	}
	if response.Error != nil {
		return nil, error.ServerError("Error Occured While Getting Order")
	}

	return &_orders, nil
}

func (r Repository) GetById(id int) (*order.Order, *error.Error) {

	_order := order.Order{}
	response := r.Repo.Where("id=?", id).First(&_order)

	if response.RowsAffected == 0 {
		return nil, error.NotFound("Order Not Found")
	}
	if response.Error != nil {
		return nil, error.ServerError("Error Occured While Getting Order")
	}

	return &_order, nil
}
