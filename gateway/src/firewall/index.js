const {verify} = require("../utils/jwt")

const isTokenNotExists = (req,res,next)=>{
    if (!req.headers.token) {
        res.status(401).json({
           status:false,
           error:"No Token Provided" 
        })
    }
    const token = verify(req.headers.token)
    req.user = token.Id
    next()
}

module.exports = {
    isTokenNotExists,
}