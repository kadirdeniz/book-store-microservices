const express = require("express")
const Router = express.Router()

const {update,get} = require("./../controller/user")

Router.put("/",update)
Router.get("/",get)

module.exports = Router