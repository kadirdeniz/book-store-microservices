const express = require("express")
const Router = express.Router()

const {create,getByUser,getByOrder,getByBook} = require("./../controller/order")

Router.post("/",create)
Router.get("/by-book/:bookId",getByBook)
Router.get("/by-order/:orderId",getByOrder)
Router.get("/by-user/:userId",getByUser)

module.exports = Router