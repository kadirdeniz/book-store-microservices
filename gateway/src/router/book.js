const express = require("express")
const Router = express.Router()

const {create,update,remove,get} = require("./../controller/book")

Router.post("/",create)

Router.get("/:bookId",get)

Router.put("/:bookId",update)

Router.delete("/:bookId",remove)

module.exports = Router