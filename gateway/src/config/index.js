module.exports = {
    "PORT":process.env.PORT,
    "SECRET_KEY":process.env.SECRET_KEY,
    "USER_BASE_URL":"http://172.18.0.3:",
    "ORDER_BASE_URL":"http://172.18.0.5:",
    "BOOK_BASE_URL":"http://172.18.0.7:",
    "BOOK":8002,
    "USER":8000,
    "ORDER":8001,
}