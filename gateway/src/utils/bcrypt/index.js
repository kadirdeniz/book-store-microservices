const bcrypt = require('bcryptjs');

const hash = async (password)=> {
    return await bcrypt.hashSync(password, await bcrypt.genSaltSync(12))
}

const compare = async (password,hashedPassword) => {
    return await bcrypt.compareSync(password, hashedPassword)
}

module.exports = {
    hash,
    compare
}