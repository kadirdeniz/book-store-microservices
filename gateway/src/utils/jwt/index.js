const jwt = require("jsonwebtoken")

const {SECRET_KEY} = require("./../../config/")

const sign = (Id)=>{
    return jwt.sign({
        Id
    },SECRET_KEY,{
        expiresIn: 1000 * 60 * 60 * 24 // a day
    })
}

const verify = (token)=>{
    return jwt.verify(token,SECRET_KEY)
}

module.exports = {
    sign,
    verify
}