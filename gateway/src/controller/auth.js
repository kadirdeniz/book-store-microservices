const { default: axios } = require("axios")
const { USER_BASE_URL, USER } = require("../config")
const {
    hash,
    compare
} = require("../utils/bcrypt")

const { sign } = require("./../utils/jwt")


const login = async (req,res)=>{
    try{
        const { email, password } = req.body

        const userByEmail = await axios.get(`${USER_BASE_URL}${USER}/user/email/${email}`,{
            headers:{
                "Content-Type":"application/x-www-form-urlencoded",
                "Accept":"*/*"
            }
        })
        const isMatch = await compare(password, userByEmail.data.data.password)
        if (!isMatch) {
            return res.json({
                status:false,
                error:"Incorrect Password"
            })
        }
        
        const token = sign(userByEmail.data.data.ID)
    
        res.json({
            status:true,
            message:"Logged In Successfully",
            data:token
        })
    }catch(err){
        console.log(err.response)
        res.json(err.response.data)
    }
}

const register = async (req,res)=>{
    try{
        let { firstname, lastname, username, email, password } = req.body
    
        password = await hash(password)

        const createUserResponse = await axios.post(
            `${USER_BASE_URL}${USER}/user/`,
            {
                firstname,
                lastname,
                username,
                email,
                password
            }
        )
        const token = sign(createUserResponse.data.data)
        createUserResponse.data.data = token
        res.json(createUserResponse.data)
    }catch(err){
        res.json(err.response.data)
    }
}

module.exports= {
    login,
    register
}