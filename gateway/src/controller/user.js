const { default: axios } = require("axios")
const { USER_BASE_URL, USER } = require("../config")

const update = async (req,res)=>{
    try{
        const userId = req.user

        let {
            firstname,
            lastname,
            email,
            username
        } = req.body

        const userUpdateResponse = await axios.put(`${USER_BASE_URL}${USER}/user/${userId}`,
            {
                firstname,
                lastname,
                email,
                username
            }
        )
        res.json(userUpdateResponse.data)

    }catch(err){
        res.json(err.response.data)
    }
}
    
const get = async (req,res)=>{
    try{
        const userId = req.user
        console.log(USER)
        const userGetResponse = await axios.get(`${USER_BASE_URL}${USER}/user/id/${userId}`)

        res.json(userGetResponse.data)

    }catch(err){
        console.log(err)
        res.json(err.response.data)
    }
}

module.exports = {
    update,
    get
}