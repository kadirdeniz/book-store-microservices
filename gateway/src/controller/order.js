const { default: axios } = require("axios")
const { ORDER_BASE_URL, ORDER } = require("../config")

const create = async (req,res)=>{
    try{

        const userId = req.user

        let {
            orderId,
            bookId
        } = req.body

        orderId = parseInt(orderId)
        bookId = parseInt(bookId)

        const createResponse = await axios.post(`${ORDER_BASE_URL}${ORDER}/order/`,
            {
                orderId,
                bookId,
                userId
            }
        )
        res.json(createResponse.data)

    }catch(err){
        res.json(err.response.data)
    }
}

const getByOrder = async (req,res)=>{
    try{
        const orderId = req.params.orderId

        const getResponse = await axios.get(`${ORDER_BASE_URL}${ORDER}/order/by-order/${orderId}`)
        res.json(getResponse.data)
    }catch(err){
        res.json(err.response.data)
    }
}

const getByUser = async (req,res)=>{
    try{
        const userId = req.params.userId

        const getResponse = await axios.get(`${ORDER_BASE_URL}${ORDER}/order/by-user/${userId}`)
        res.json(getResponse.data)
    }catch(err){
        res.json(err.response.data)
    }
}

const getByBook = async (req,res)=>{
    try{
        const bookId = req.params.bookId

        const getResponse = await axios.get(`${ORDER_BASE_URL}${ORDER}/order/by-book/${bookId}`)
        res.json(getResponse.data)
    }catch(err){
        res.json(err.response.data)
    }
}

module.exports = {
    create,
    getByBook,
    getByOrder,
    getByUser
}