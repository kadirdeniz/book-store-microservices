const { default: axios } = require("axios")

const {
    BOOK_BASE_URL,
    BOOK
} = require("./../config/")

const create = async (req,res)=>{
    try{
        let {
            name,
            author,
            page,
            stock
        } = req.body
        page = parseInt(page)
        stock = parseInt(stock)
        const createResponse = await axios.post(`${BOOK_BASE_URL}${BOOK}/book/`, 
            {
                name,
                author,
                page,
                stock
            }
        )
        res.json(createResponse.data)
    }catch(err){
        console.log(err)
        res.json(err.response.data)
    }
}

const update = async (req,res)=>{
    try{
        let {
            name,
            author,
            page,
            stock
        } = req.body
        page = parseInt(page)
        stock = parseInt(stock)

        const bookId = req.params.bookId

        const updateResponse = await axios.put(`${BOOK_BASE_URL}${BOOK}/book/${bookId}`, 
            {
                name,
                author,
                page,
                stock
            }
        )
        res.json(updateResponse.data)
    }catch(err){
        res.json(err.response.data)
    }
}

const remove = async (req,res)=>{
    try{

        const bookId = req.params.bookId

        const deleteResponse = await axios.delete(`${BOOK_BASE_URL}${BOOK}/book/${bookId}`)
        res.json(deleteResponse.data)
    }catch(err){
        res.json(err.response.data)
    }
}

const get = async (req,res)=>{
    try{

        const bookId = req.params.bookId
        const getResponse = await axios.get(`${BOOK_BASE_URL}${BOOK}/book/${bookId}`,
        {
            "Accept":"*/*"
        })
        res.json(getResponse.data)
    }catch(err){
        res.json(err.response.data)
    }
}

module.exports={
    create,
    update,
    remove,
    get
}