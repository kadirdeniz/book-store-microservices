const express = require("express")
const app = express()

const bodyParser = require('body-parser')

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

const {isTokenNotExists} = require("./firewall/")

const {PORT} = require("./config")
const authRoutes = require("./router/auth")
const bookRoutes = require("./router/book")
const userRoutes = require("./router/user")
const orderRoutes = require("./router/order")

app.use(express.json())

app.use("/auth",authRoutes)
app.use("/book", isTokenNotExists, bookRoutes)
app.use("/user", isTokenNotExists, userRoutes)
app.use("/order", isTokenNotExists, orderRoutes)

app.listen(PORT,()=>console.log(`Server Started at ${PORT}`))