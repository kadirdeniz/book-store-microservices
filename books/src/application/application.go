package application

import (
	"fmt"
	"log"
	"mamazon/book/src/book"
	"mamazon/book/src/config"
	"mamazon/book/src/database/postgresql"
	"mamazon/book/src/handler"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
)

func Setup() {
	postgresql.Migrate(book.Book{})
	app := fiber.New()

	app.Use(cors.New())

	book := app.Group("/book")
	book.Post("/", handler.Create)
	book.Get("/:bookId", handler.GetById)
	book.Put("/:bookId", handler.Update)
	book.Delete("/:bookId", handler.Delete)

	log.Fatal(app.Listen(fmt.Sprintf(":%v", config.PORT)))
}
