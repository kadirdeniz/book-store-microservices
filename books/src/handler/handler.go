package handler

import (
	"fmt"
	"mamazon/book/src/book"
	bookService "mamazon/book/src/book/service"

	"github.com/gofiber/fiber/v2"
)

func Create(c *fiber.Ctx) error {

	bookService := bookService.New()

	_book := book.Book{}

	if err := c.BodyParser(&_book); err != nil {
		return c.Status(fiber.ErrBadRequest.Code).JSON(fiber.Map{
			"status": false,
			"error":  err,
		})
	}

	if _book.GetName() == "" || _book.GetAuthor() == "" || _book.GetPage() == 0 || _book.GetStock() == 0 {
		return c.Status(fiber.ErrBadRequest.Code).JSON(fiber.Map{
			"status": false,
			"error":  "All Fields Must Be Filled",
		})
	}

	createErr := bookService.Create(_book.GetName(), _book.GetAuthor(), _book.GetPage(), _book.GetStock())
	if createErr != nil {
		return c.Status(createErr.StatusCode).JSON(fiber.Map{
			"status": false,
			"error":  createErr,
		})
	}
	return c.JSON(fiber.Map{
		"status":  true,
		"message": "Book Created",
	})
}

func Update(c *fiber.Ctx) error {

	bookService := bookService.New()

	_book := book.Book{}

	if err := c.BodyParser(&_book); err != nil {
		return c.Status(fiber.ErrBadRequest.Code).JSON(fiber.Map{
			"status": false,
			"error":  err,
		})
	}

	id, paramsErr := c.ParamsInt("bookId")
	if paramsErr != nil {
		return c.Status(fiber.ErrBadRequest.Code).JSON(fiber.Map{
			"status": false,
			"error":  "Parameter Should Be Integer",
		})
	}

	updateErr := bookService.Update(id, _book.GetName(), _book.GetAuthor(), _book.GetPage(), _book.GetStock())
	if updateErr != nil {
		return c.Status(updateErr.StatusCode).JSON(fiber.Map{
			"status": false,
			"error":  updateErr,
		})
	}
	return c.JSON(fiber.Map{
		"status":  true,
		"message": "Book Updated",
	})
}

func Delete(c *fiber.Ctx) error {

	bookService := bookService.New()

	id, paramsErr := c.ParamsInt("bookId")
	if paramsErr != nil {
		return c.Status(fiber.ErrBadRequest.Code).JSON(fiber.Map{
			"status": false,
			"error":  "Parameter Should Be Integer",
		})
	}

	deleteErr := bookService.Delete(id)
	if deleteErr != nil {
		return c.Status(deleteErr.StatusCode).JSON(fiber.Map{
			"status": false,
			"error":  deleteErr,
		})
	}
	return c.JSON(fiber.Map{
		"status":  true,
		"message": "Book Deleted",
	})
}

func GetById(c *fiber.Ctx) error {

	bookService := bookService.New()

	id, paramsErr := c.ParamsInt("bookId")
	if paramsErr != nil {
		return c.Status(fiber.ErrBadRequest.Code).JSON(fiber.Map{
			"status": false,
			"error":  "Parameter Should Be Integer",
		})
	}

	book, getErr := bookService.GetById(id)
	if getErr != nil {
		fmt.Println(getErr)
		return c.Status(getErr.StatusCode).JSON(fiber.Map{
			"status": false,
			"error":  getErr,
		})
	}
	return c.JSON(fiber.Map{
		"status":  true,
		"message": "Got Book",
		"data":    book,
	})
}
