package error

type Error struct {
	StatusCode int    `json:"status_code"`
	Type       string `json:"error_type"`
	Message    string `json:"error_message"`
}

func BadRequest(message string) *Error {
	return &Error{
		StatusCode: 400,
		Type:       "bad_request",
		Message:    message,
	}
}

func NotFound(message string) *Error {
	return &Error{
		StatusCode: 404,
		Type:       "not_found",
		Message:    message,
	}
}
