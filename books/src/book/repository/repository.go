package repository

import (
	"mamazon/book/src/book"
	"mamazon/book/src/database/postgresql"
	error "mamazon/book/src/utils/errors"

	"gorm.io/gorm"
)

type Repository struct {
	Repo *gorm.DB
}

func New() IRepository {
	database, err := postgresql.Connect()
	if err != nil {
		panic(err)
	}
	return Repository{
		Repo: database,
	}
}

func (r Repository) Create(name, author string, page, stock int) *error.Error {
	_book := book.Book{
		Name:   name,
		Author: author,
		Page:   page,
		Stock:  stock,
	}
	response := r.Repo.Create(&_book)

	if response.Error != nil {
		return error.BadRequest("Error Occured While Creating Error")
	}
	return nil

}

func (r Repository) Update(id int, name, author string, page, stock int) *error.Error {
	_book := book.Book{
		Name:   name,
		Author: author,
		Page:   page,
		Stock:  stock,
	}
	response := r.Repo.Where("id=?", id).Updates(&_book)

	if response.Error != nil && response.RowsAffected == 0 {
		return error.NotFound("Book Not Found")
	}

	if response.Error != nil {
		return error.BadRequest("Error Occured While Creating Error")
	}
	return nil

}
func (r Repository) Delete(id int) *error.Error {
	response := r.Repo.Delete(&book.Book{}, id)
	if response.Error != nil && response.RowsAffected == 0 {
		return error.NotFound("Book Not Found")
	}
	if response.Error != nil {
		return error.BadRequest("Error Occured While Deleting Error")
	}
	return nil
}
func (r Repository) GetById(id int) (*book.Book, *error.Error) {
	_book := book.Book{}
	response := r.Repo.First(&_book, "id=?", id)
	if response.Error != nil && response.RowsAffected == 0 {
		return nil, error.NotFound("Book Not Found")
	}
	if response.Error != nil {
		return nil, error.ServerError("Error Occured While Getting Book")
	}

	return &_book, nil
}
