package repository

import (
	"mamazon/book/src/book"
	error "mamazon/book/src/utils/errors"
)

type IRepository interface {
	Create(name, author string, page, stock int) *error.Error
	Update(id int, name, author string, page, stock int) *error.Error
	Delete(id int) *error.Error
	GetById(id int) (*book.Book, *error.Error)
}
