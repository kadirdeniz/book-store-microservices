package book

import "gorm.io/gorm"

type Book struct {
	gorm.Model
	Name   string `json:"name"`
	Author string `json:"author"`
	Page   int    `json:"page"`
	Stock  int    `json:"stock"`
}

func (b Book) GetName() string {
	return b.Name
}

func (b Book) GetAuthor() string {
	return b.Name
}

func (b Book) GetPage() int {
	return b.Page
}

func (b Book) GetStock() int {
	return b.Stock
}

func (b Book) SetName(name string) {
	b.Name = name
}

func (b Book) SetAuthor(author string) {
	b.Author = author
}

func (b Book) SetPage(page int) {
	b.Page = page
}

func (b Book) SetStock(stock int) {
	b.Stock = stock
}
