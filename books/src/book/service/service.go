package service

import (
	"mamazon/book/src/book"
	"mamazon/book/src/book/repository"
	error "mamazon/book/src/utils/errors"
)

type Service struct {
	Repo repository.IRepository
}

func New() IService {
	return Service{
		Repo: repository.New(),
	}
}

func (s Service) Create(name, author string, page, stock int) *error.Error {
	err := s.Repo.Create(name, author, page, stock)
	if err != nil {
		return err
	}
	return nil
}
func (s Service) Update(id int, name, author string, page, stock int) *error.Error {
	err := s.Repo.Update(id, name, author, page, stock)
	if err != nil {
		return err
	}
	return nil
}
func (s Service) Delete(id int) *error.Error {
	err := s.Repo.Delete(id)
	if err != nil {
		return err
	}
	return nil
}
func (s Service) GetById(id int) (*book.Book, *error.Error) {
	book, err := s.Repo.GetById(id)
	if err != nil {
		return nil, err
	}
	return book, nil
}
