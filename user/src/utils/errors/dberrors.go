package error

func ServerError(message string) *Error {
	return &Error{
		StatusCode: 500,
		Type:       "internal_server_error",
		Message:    message,
	}
}
