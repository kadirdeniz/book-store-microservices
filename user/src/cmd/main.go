package main

import (
	"mamazon/user/src/application"
)

func main() {
	application.Start()
}
