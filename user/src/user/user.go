package user

import "gorm.io/gorm"

type User struct {
	*gorm.Model
	FirstName string `json:"firstname"`
	LastName  string `json:"lastname"`
	Username  string `json:"username"`
	Email     string `json:"email"`
	Password  string `json:"password"`
}

func New(user User) User {
	return user
}

func (u User) GetFirstName() string {
	return u.FirstName
}

func (u User) GetLastName() string {
	return u.LastName
}

func (u User) GetUsername() string {
	return u.Username
}

func (u User) GetEmail() string {
	return u.Email
}

func (u User) GetPassword() string {
	return u.Password
}

func (u User) SetPassword(password string) {
	u.Password = password
}

func (u User) SetEmail(email string) {
	u.Email = email
}

func (u User) SetUsername(username string) {
	u.Username = username
}

func (u User) SetLastName(last_name string) {
	u.LastName = last_name
}

func (u User) SetFirstName(first_name string) {
	u.FirstName = first_name
}
