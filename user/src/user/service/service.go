package service

import (
	"mamazon/user/src/user"
	"mamazon/user/src/user/repository"
	error "mamazon/user/src/utils/errors"
)

type Service struct {
	repo repository.IRepository
}

func New() IService {
	return Service{
		repo: repository.New(),
	}
}

func (s Service) Create(name, surname, nickname, email, password string) (*error.Error, *uint) {
	err, userId := s.repo.Create(name, surname, nickname, email, password)
	if err != nil {
		return err, nil
	}
	return nil, userId
}

func (s Service) Update(id int, name, surname, nickname, email string) *error.Error {
	err := s.repo.Update(id, name, surname, nickname, email)
	if err != nil {
		return err
	}
	return nil
}

func (s Service) Delete(id int) *error.Error {
	err := s.repo.Delete(id)
	if err != nil {
		return err
	}
	return nil
}

func (s Service) GetById(id int) (*user.User, *error.Error) {
	_user, err := s.repo.GetById(id)
	if err != nil {
		return nil, err
	}
	return _user, nil
}

func (s Service) GetByEmail(email string) (*user.User, *error.Error) {
	_user, err := s.repo.GetByEmail(email)
	if err != nil {
		return nil, err
	}
	return _user, nil
}
