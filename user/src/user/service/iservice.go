package service

import (
	"mamazon/user/src/user"
	error "mamazon/user/src/utils/errors"
)

type IService interface {
	Create(name, surname, nickname, email, password string) (*error.Error, *uint)
	Update(id int, name, surname, nickname, email string) *error.Error
	Delete(id int) *error.Error
	GetById(id int) (*user.User, *error.Error)
	GetByEmail(email string) (*user.User, *error.Error)
}
