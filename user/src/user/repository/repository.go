package repository

import (
	"fmt"
	"mamazon/user/src/database"
	"mamazon/user/src/user"
	error "mamazon/user/src/utils/errors"

	"gorm.io/gorm"
)

type Repository struct {
	repo *gorm.DB
}

func New() IRepository {

	DB, err := database.Connect()
	if err != nil {
		panic(err)
	}

	return Repository{
		repo: DB,
	}
}

func (r Repository) Create(name, surname, nickname, email, password string) (*error.Error, *uint) {
	_user := user.New(user.User{
		FirstName: name,
		LastName:  surname,
		Username:  nickname,
		Email:     email,
		Password:  password,
	})

	response := r.repo.Create(&_user)

	if response.Error != nil {
		return error.ServerError("Error Occured While Creating User"), nil
	}
	return nil, &_user.ID
}

func (r Repository) Delete(id int) *error.Error {
	response := r.repo.Delete(&user.User{}, id)
	if response.RowsAffected == 0 {
		return error.UserNotFound("User Not Found")
	}
	if response.Error != nil {
		return error.ServerError("Error Occured While Deleting User")
	}
	return nil
}

func (r Repository) Update(id int, name, surname, nickname, email string) *error.Error {
	_user := user.New(user.User{
		FirstName: name,
		LastName:  surname,
		Username:  nickname,
		Email:     email,
	})
	response := r.repo.Model(&user.User{}).Where("id=?", id).Updates(_user)

	if response.RowsAffected == 0 {
		return error.UserNotFound("User Not Found")
	}
	if response.Error != nil {
		return error.ServerError("Error Occured While Updating User")
	}

	return nil
}

func (r Repository) GetById(id int) (*user.User, *error.Error) {
	_user := user.New(user.User{})
	response := r.repo.First(&_user, "id=?", id)
	if response.RowsAffected == 0 {
		return nil, error.UserNotFound("User Not Found")
	}
	if response.Error != nil {
		return nil, error.ServerError("Error Occured While Getting User")
	}

	return &_user, nil
}

func (r Repository) GetByEmail(email string) (*user.User, *error.Error) {
	_user := user.New(user.User{})
	response := r.repo.First(&_user, "email=?", email)

	if response.RowsAffected == 0 {
		return nil, error.UserNotFound("User Not Found")
	}
	if response.Error != nil {
		fmt.Println(response)
		return nil, error.ServerError("Error Occured While Getting User")
	}

	return &_user, nil
}
