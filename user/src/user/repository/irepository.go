package repository

import (
	"mamazon/user/src/user"
	error "mamazon/user/src/utils/errors"
)

type IRepository interface {
	Create(name, surname, nickname, email, password string) (*error.Error, *uint)
	Delete(id int) *error.Error
	Update(id int, name, surname, nickname, email string) *error.Error
	GetById(id int) (*user.User, *error.Error)
	GetByEmail(email string) (*user.User, *error.Error)
}
