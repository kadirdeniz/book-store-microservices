package application

import (
	"log"
	"mamazon/user/src/database"
	"mamazon/user/src/handler"
	"mamazon/user/src/user"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
)

func Start() {

	database.Migrate(user.User{})

	app := fiber.New()

	app.Use(cors.New())

	app.Get("/", handler.Hello)
	user := app.Group("/user")
	user.Post("/", handler.Create)
	user.Get("/id/:userId", handler.GetById)
	user.Get("/email/:email", handler.GetByEmail)
	user.Put("/:userId", handler.Update)
	user.Delete("/:userId", handler.Delete)

	log.Fatal(app.Listen(":8000"))
}
