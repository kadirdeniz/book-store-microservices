package database

import (
	"fmt"
	"mamazon/user/src/config"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var dsn = fmt.Sprintf("host=%v user=%v password=%v dbname=%v port=%v",
	config.POSTGRES_HOST,
	config.POSTGRES_USER,
	config.POSTGRES_PASSWORD,
	config.POSTGRES_DB,
	config.POSTGRES_PORT,
)

func Connect() (*gorm.DB, error) {
	fmt.Println(dsn)
	DB, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		return nil, err
	}
	return DB, nil
}

func DisConnect(db *gorm.DB) error {
	database, err := db.DB()
	if err != nil {
		return err
	}
	database.Close()
	return nil
}

func hasTable(db *gorm.DB, table interface{}) bool {
	return db.Migrator().HasTable(table)
}

func createTable(db *gorm.DB, table interface{}) error {
	err := db.Migrator().CreateTable(table)
	if err != nil {
		return err
	}
	return nil
}

func Migrate(table interface{}) {
	db, err := Connect()
	if err != nil {
		panic(err)
	}
	fmt.Println("Database Connection Success")
	isTableExists := hasTable(db, table)
	if !isTableExists {
		createTable(db, table)
	}
	defer DisConnect(db)
}
