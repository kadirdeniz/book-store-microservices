package handler

import (
	"mamazon/user/src/user"
	userService "mamazon/user/src/user/service"

	"github.com/gofiber/fiber/v2"
)

func Create(c *fiber.Ctx) error {

	userService := userService.New()

	_user := user.New(user.User{})

	if err := c.BodyParser(&_user); err != nil {
		return c.Status(fiber.ErrBadRequest.Code).JSON(fiber.Map{
			"status": false,
			"error":  err,
		})
	}

	if _user.GetFirstName() == "" || _user.GetLastName() == "" || _user.GetUsername() == "" || _user.GetEmail() == "" || _user.GetPassword() == "" {
		return c.Status(fiber.ErrBadRequest.Code).JSON(fiber.Map{
			"status": false,
			"error":  "All Fields Must Be Filled",
		})
	}

	createErr, userId := userService.Create(_user.GetFirstName(), _user.GetLastName(), _user.GetUsername(), _user.GetEmail(), _user.GetPassword())
	if createErr != nil {
		return c.Status(createErr.StatusCode).JSON(fiber.Map{
			"status": false,
			"error":  createErr,
		})
	}
	return c.JSON(fiber.Map{
		"status":  true,
		"message": "User Created",
		"data":    userId,
	})
}

func Update(c *fiber.Ctx) error {

	userService := userService.New()

	_user := user.New(user.User{})

	if err := c.BodyParser(&_user); err != nil {
		return c.Status(fiber.ErrBadRequest.Code).JSON(fiber.Map{
			"status": false,
			"error":  err,
		})
	}

	id, paramsErr := c.ParamsInt("userId")
	if paramsErr != nil {
		return c.Status(fiber.ErrBadRequest.Code).JSON(fiber.Map{
			"status": false,
			"error":  "Parameter Should Be Integer",
		})
	}

	updateErr := userService.Update(id, _user.GetFirstName(), _user.GetLastName(), _user.GetUsername(), _user.GetEmail())
	if updateErr != nil {
		return c.Status(updateErr.StatusCode).JSON(fiber.Map{
			"status": false,
			"error":  updateErr,
		})
	}
	return c.JSON(fiber.Map{
		"status":  true,
		"message": "User Updated",
	})
}

func Delete(c *fiber.Ctx) error {

	userService := userService.New()

	id, paramsErr := c.ParamsInt("userId")
	if paramsErr != nil {
		return c.Status(fiber.ErrBadRequest.Code).JSON(fiber.Map{
			"status": false,
			"error":  "Parameter Should Be Integer",
		})
	}

	deleteErr := userService.Delete(id)
	if deleteErr != nil {
		return c.Status(deleteErr.StatusCode).JSON(fiber.Map{
			"status": false,
			"error":  deleteErr,
		})
	}
	return c.JSON(fiber.Map{
		"status":  true,
		"message": "User Deleted",
	})
}

func GetById(c *fiber.Ctx) error {

	userService := userService.New()

	id, paramsErr := c.ParamsInt("userId")
	if paramsErr != nil {
		return c.Status(fiber.ErrBadRequest.Code).JSON(fiber.Map{
			"status": false,
			"error":  "Parameter Should Be Integer",
		})
	}

	user, getErr := userService.GetById(id)
	if getErr != nil {
		return c.Status(getErr.StatusCode).JSON(fiber.Map{
			"status": false,
			"error":  getErr,
		})
	}
	return c.JSON(fiber.Map{
		"status":  true,
		"message": "Got User",
		"data":    user,
	})
}

func GetByEmail(c *fiber.Ctx) error {

	userService := userService.New()

	email := c.Params("email")

	user, getErr := userService.GetByEmail(email)
	if getErr != nil {
		return c.Status(getErr.StatusCode).JSON(fiber.Map{
			"status": false,
			"error":  getErr,
		})
	}
	return c.JSON(fiber.Map{
		"status":  true,
		"message": "Got User",
		"data":    user,
	})
}

func Hello(c *fiber.Ctx) error {
	return c.JSON(fiber.Map{
		"status":  true,
		"message": "Hello World",
	})
}
