package config

import "os"

var (
	PORT              = os.Getenv("PORT")
	POSTGRES_PORT     = os.Getenv("POSTGRES_PORT")
	POSTGRES_DB       = os.Getenv("POSTGRES_DB")
	POSTGRES_USER     = os.Getenv("POSTGRES_USER")
	POSTGRES_PASSWORD = os.Getenv("POSTGRES_PASSWORD")
	POSTGRES_HOST     = os.Getenv("POSTGRES_HOST")
)
